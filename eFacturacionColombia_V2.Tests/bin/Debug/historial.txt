TIPO		FECHA		NUMERO		CUFE		ESTADO

FACTURA_VENTA		15/08/2020 9:30:28 a. m.		SETP990000001		d62252c06463820400a9e21aec59cdd0ce2e348bdb506c9447bc5c2a1665328cbf5bad5a9ae124916262fd01591b1ccc		2: 

NOTA_CREDITO		15/08/2020 9:30:45 a. m.		SETP990000002		a93347c18cbd8441a7847b9315b95d0604e67a20b82a4650f250a92d01bee8f500d8d9366ff63e9c8c8076a625cf4be8		2: 

FACTURA_VENTA		15/08/2020 9:40:53 a. m.		SETP990000003		20d796eb5f13d0f6c036ca7f58e111e25650e82c3bd3ba5af8998e1a07f6b12e0023f2ce1bb4e76c328acab8631b6b78		2: 

NOTA_CREDITO		15/08/2020 9:41:12 a. m.		SETP990000004		687a29172b5cf11c3b91434a2d6c0e8a2c1ad0258531e12ad825e30e66a2d76416d49944b309053cf615fc9f10a65414		2: 

FACTURA_VENTA		15/08/2020 12:31:38 p. m.		SETP990010001		52f647fc07cdb4d2530c0e5bca71f06355398dfabcae5b065103accddf942fc19b681e8c814b9c19d4437c38f264a833		00: La Factura electrónica SETP-990010001, ha sido autorizada.

NOTA_CREDITO		15/08/2020 12:33:14 p. m.		SETP990010002		dab6111a7cbb86174e8c2a0f45ffbebc2f020a9d8ff71f57c8be79ff8722a96c9997316d40eb2074423a92b2fbf66458		00: La Nota de crédito electrónica SETP-990010002, ha sido autorizada.

FACTURA_VENTA		15/08/2020 1:04:35 p. m.		SETP990010005		964f083eb529f62b21a2586044a9566d6f925f3c742366c9092d91f8eb7c3b734bcf32bc6ac0ad5617039efd27166125		0: 

NOTA_CREDITO		15/08/2020 1:04:48 p. m.		SETP990010006		3bb8a972b4e5f128814c9e37848f33076acf08d7e2798ea4947d6be2dce8b6c9f2bd1b524c3a2e03c77e893778aadadd		0: 

FACTURA_VENTA		15/08/2020 1:40:37 p. m.		SETP990010009		49d8540470e65b468d77168ef33e61e57d5656928aa2d07e8b4a824221d4eda03caeab90e00e7843982d2a3ca1cc20a1		: 

NOTA_CREDITO		15/08/2020 1:41:11 p. m.		SETP990010010		67aa5e90c22ef9270374bcfe242494b028b3976e6754a6af0395c8f8d076dd837c9e7d11a9dbadee2376fe58e88a2e17		0: 

FACTURA_VENTA		15/08/2020 1:42:08 p. m.		SETP990010011		0cb23f629ffde510516709d71743968e85ad2f694ea95d81276772d6f35ccf0216c68640d323e0813d805a2b710c0c14		: 

NOTA_CREDITO		15/08/2020 1:42:40 p. m.		SETP990010012		520f23349330fdbef8224988c98f236305fe7f7ba53b51242a37158b7ab7c649dc31b7ec70ba378361e5f650aaaf29c0		0: 

FACTURA_VENTA		15/08/2020 1:50:12 p. m.		SETP990010013		f5414e5e286d89d2dff5174ffcfe9164886cb232d4803b3053cc9ba24c6f9c2289439792d3d88c2bee7f9435077674aa		00: La Factura electrónica SETP-990010013, ha sido autorizada.

NOTA_CREDITO		15/08/2020 1:50:51 p. m.		SETP990010014		5e3b08bf14987cba5a773b789b7a4f1dc5800b28f3fad0bb62231687639fc1d5e7c0b43e1143f9b3483940ea551da89a		0: 

FACTURA_VENTA		15/08/2020 1:59:37 p. m.		SETP990010015		f1412b734c152a939621d55a7606f79cd437ec2650250cbbd77f9a63a53528f72dc9e3296f26e4a37af74ee3265cbeef		99: Documento con errores en campos mandatorios.
Regla: FAK26, Rechazo: Responsabilidad informada para receptor no valida según lista.
Regla: ZB01 , Rechazo: Fallo en el Schema XML del archivo  - The element 'Invoice' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2' has invalid child element 'AccountingCustomerParty' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'. List of possible elements expected: 'PayeeParty, BuyerCustomerParty, SellerSupplierParty, TaxRepresentativeParty, Delivery, DeliveryTerms, PaymentMeans, PaymentTerms, PrepaidPayment, AllowanceCharge, TaxExchangeRate, PricingExchangeRate, PaymentExchangeRate, PaymentAlternativeExchangeRate, TaxTotal, WithholdingTaxTotal, LegalMonetaryTotal' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'.
Regla: FAK60, Notificación: La sumatoria de los porcentajes de participación debe ser 100
Regla: FAZ09, Notificación: Debe existir el grupo de información de identificación del bien o servicio

NOTA_CREDITO		15/08/2020 2:00:47 p. m.		SETP990010016		502395cae230ffad77d6849d4f5ba14962ddd07a8e83c075baf64837e58b92ba552dc1fb33201f6278eb5920dbde8b20		99: Documento con errores en campos mandatorios.
Regla: CBG04a, Rechazo: Documento referenciado no existe en los registros de la DIAN.

FACTURA_VENTA		15/08/2020 2:02:15 p. m.		SETP990010017		5940943e234959bfcbe1e13af81a2b202b3867e85800d4aec4a38ab2459efec7c177176ed0a1a266e28550613c6bf16a		99: Documento con errores en campos mandatorios.
Regla: FAK26, Rechazo: Responsabilidad informada para receptor no valida según lista.
Regla: ZB01 , Rechazo: Fallo en el Schema XML del archivo  - The element 'Invoice' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2' has invalid child element 'AccountingCustomerParty' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'. List of possible elements expected: 'PayeeParty, BuyerCustomerParty, SellerSupplierParty, TaxRepresentativeParty, Delivery, DeliveryTerms, PaymentMeans, PaymentTerms, PrepaidPayment, AllowanceCharge, TaxExchangeRate, PricingExchangeRate, PaymentExchangeRate, PaymentAlternativeExchangeRate, TaxTotal, WithholdingTaxTotal, LegalMonetaryTotal' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'.
Regla: FAZ09, Notificación: Debe existir el grupo de información de identificación del bien o servicio

NOTA_CREDITO		15/08/2020 2:03:00 p. m.		SETP990010018		71a5092f0a91038e2942ac8e997ec258d68d4b4296da447c272a5de58e4211d5b5515f978f97df8533f2e1821af7fc15		99: Documento con errores en campos mandatorios.
Regla: CBG04a, Rechazo: Documento referenciado no existe en los registros de la DIAN.

FACTURA_VENTA		15/08/2020 2:53:55 p. m.		SETP990010019		4e42af0cf0a0f8c40d78d1acb266f84fff501f66acea19350ffa168548a0e28f25d13cf43057d773f046c88d19fd25b8		99: Documento con errores en campos mandatorios.
Regla: FAK26, Rechazo: Responsabilidad informada para receptor no valida según lista.
Regla: ZB01 , Rechazo: Fallo en el Schema XML del archivo  - The element 'Invoice' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2' has invalid child element 'AccountingCustomerParty' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'. List of possible elements expected: 'PayeeParty, BuyerCustomerParty, SellerSupplierParty, TaxRepresentativeParty, Delivery, DeliveryTerms, PaymentMeans, PaymentTerms, PrepaidPayment, AllowanceCharge, TaxExchangeRate, PricingExchangeRate, PaymentExchangeRate, PaymentAlternativeExchangeRate, TaxTotal, WithholdingTaxTotal, LegalMonetaryTotal' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'.
Regla: FAK60, Notificación: La sumatoria de los porcentajes de participación debe ser 100
Regla: FAZ09, Notificación: Debe existir el grupo de información de identificación del bien o servicio

NOTA_CREDITO		15/08/2020 2:54:16 p. m.		SETP990010020		b5e2f7a1b1f59ffd4e1b23684a2659275ad6324eb7dc4a65131fd81db4c968c30905b3a052e127b1a2315bb3131e3e99		0: 

FACTURA_VENTA		15/08/2020 2:54:40 p. m.		SETP990010021		a1843ae2620ac80adf710212866ac21eff9086e558ab66c80efda8856caf8518db3866725924eb999d41aefac1cd706c		99: Documento con errores en campos mandatorios.
Regla: FAK26, Rechazo: Responsabilidad informada para receptor no valida según lista.
Regla: ZB01 , Rechazo: Fallo en el Schema XML del archivo  - The element 'Invoice' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2' has invalid child element 'AccountingCustomerParty' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'. List of possible elements expected: 'PayeeParty, BuyerCustomerParty, SellerSupplierParty, TaxRepresentativeParty, Delivery, DeliveryTerms, PaymentMeans, PaymentTerms, PrepaidPayment, AllowanceCharge, TaxExchangeRate, PricingExchangeRate, PaymentExchangeRate, PaymentAlternativeExchangeRate, TaxTotal, WithholdingTaxTotal, LegalMonetaryTotal' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'.
Regla: FAZ09, Notificación: Debe existir el grupo de información de identificación del bien o servicio

NOTA_CREDITO		15/08/2020 2:54:58 p. m.		SETP990010022		28932c07b3af2c129e9f16d500c510df89513b015f0fbdb19d0261492b7fcd9bc42330a677c48d631c15a8dcb1f0c1e1		0: 

FACTURA_VENTA		15/08/2020 2:56:33 p. m.		SETP990010023		e8183fe94035eef4c846d2ca7e359c0492cd741741bfa996883669d0e2442607b35e8ded9a13cd0ffb821d9c58346733		99: Documento con errores en campos mandatorios.
Regla: FAK26, Rechazo: Responsabilidad informada para receptor no valida según lista.
Regla: ZB01 , Rechazo: Fallo en el Schema XML del archivo  - The element 'Invoice' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2' has invalid child element 'AccountingCustomerParty' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'. List of possible elements expected: 'PayeeParty, BuyerCustomerParty, SellerSupplierParty, TaxRepresentativeParty, Delivery, DeliveryTerms, PaymentMeans, PaymentTerms, PrepaidPayment, AllowanceCharge, TaxExchangeRate, PricingExchangeRate, PaymentExchangeRate, PaymentAlternativeExchangeRate, TaxTotal, WithholdingTaxTotal, LegalMonetaryTotal' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'.
Regla: FAZ09, Notificación: Debe existir el grupo de información de identificación del bien o servicio

NOTA_CREDITO		15/08/2020 2:56:47 p. m.		SETP990010024		b97328288cd5939da40539bc9abe67bcc3e79c0678ccdf5ac7d4133957de66f0543166ed43bc22608db832ff2fe8859c		0: 

FACTURA_VENTA		15/08/2020 2:58:18 p. m.		SETP990010025		40b78380639453fba65962181b7cb0b7bbdc0fb331a136b4820dff1265283b6978271f7b56d20a0e972bbbbb4af0da33		99: Documento con errores en campos mandatorios.
Regla: FAK26, Rechazo: Responsabilidad informada para receptor no valida según lista.
Regla: ZB01 , Rechazo: Fallo en el Schema XML del archivo  - The element 'Invoice' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:Invoice-2' has invalid child element 'AccountingCustomerParty' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'. List of possible elements expected: 'PayeeParty, BuyerCustomerParty, SellerSupplierParty, TaxRepresentativeParty, Delivery, DeliveryTerms, PaymentMeans, PaymentTerms, PrepaidPayment, AllowanceCharge, TaxExchangeRate, PricingExchangeRate, PaymentExchangeRate, PaymentAlternativeExchangeRate, TaxTotal, WithholdingTaxTotal, LegalMonetaryTotal' in namespace 'urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2'.
Regla: FAZ09, Notificación: Debe existir el grupo de información de identificación del bien o servicio

NOTA_CREDITO		15/08/2020 2:58:33 p. m.		SETP990010026		7f089a1481188389424e6cc6c79f7fcdea8be33fba9b67228603b6b3f9968bec91e2278b7aef8f0c5ecef816debb39ae		0: 

FACTURA_VENTA		15/08/2020 3:09:39 p. m.		SETP990010027		69cc0b6a17447b899d989b7788b9d42f16e8104a800a3ac5fb092a88637001d24470bd3e520c113ba412a641d9e90c11		00: La Factura electrónica SETP-990010027, ha sido autorizada.

NOTA_CREDITO		15/08/2020 3:10:22 p. m.		SETP990010028		0ae5a2b4d679b54aaf6da35428169322fe2c766e4edaaeb1b892f892fad48180a488eb712fb74f55aeb42540683661cf		00: La Nota de crédito electrónica SETP-990010028, ha sido autorizada.

FACTURA_VENTA		15/08/2020 3:41:29 p. m.		SETP990010029		5864c3e67ad0f7caed23799afbafa41c2224be28d9f891098b1cb98332258a2b70911f4b5da7c9636ba03c3ff11e60e0		00: La Factura electrónica SETP-990010029, ha sido autorizada.

NOTA_CREDITO		15/08/2020 3:41:41 p. m.		SETP990010030		98a76c8689b36d8feba9afc1e59e4ea045395b7bfacb077ad875dce9828eff52cf4ff3d80abcde07ce275a980c709bf0		00: La Nota de crédito electrónica SETP-990010030, ha sido autorizada.

FACTURA_VENTA		15/08/2020 3:44:42 p. m.		SETP990010031		ac2e27ed433880fff87d7696b398b21341f5dd504dc61968c4c466a0aeb5f916aa78c5c46b9595ff6cf7b33e4818501e		00: La Factura electrónica SETP-990010031, ha sido autorizada.

FACTURA_VENTA		15/08/2020 4:01:50 p. m.		SETP990010032		6d1dc5183e765fb7f84eb25cccca4bdd2ec3fe62334a42a3a9adaa73af6d0eca878a4c8cb870f23bab20433764b74ffa		00: La Factura electrónica SETP-990010032, ha sido autorizada.

NOTA_CREDITO		15/08/2020 4:04:23 p. m.		SETP990010033		05e46a4dbf2e89debe61862ceca26cc8122684575fe7d17b0f3ea42be84e1773a41f0594af79c4226b243226badb3834		00: La Nota de crédito electrónica SETP-990010033, ha sido autorizada.

FACTURA_EXPORTACION		18/08/2020 3:21:38 p. m.		SETP990010034		ccecc3931f8842e2eba8aadb45189868e9714d123bfe3ff2dccea4ac22964a0c1960b465e6f70314106de0fbe6a11adb		00: La Factura electrónica de exportación SETP-990010034, ha sido autorizada.

FACTURA_EXPORTACION		18/08/2020 4:03:28 p. m.		SETP990010035		014236e0350738ed6e420e224f3f0f589dafb368865f4a5bc8bf27f26e74f702486dcce6785c7f49f56d32fd9816b0aa		: 

FACTURA_EXPORTACION		18/08/2020 4:06:03 p. m.		SETP990010036		be86a0a0775cf50891722e36648720cb3a8f82001c51add3e281d5e69ffabc45a6d0ec2e13aa56df5bfbcc6276932a29		0: 

FACTURA_VENTA		11/09/2020 6:33:02 p. m.		SETP990000975		57d7b4d8aed7e5a02a48882653d45d840b2547106f42d38c9f9ac03b9c2603d49fe7a75a8947c01c2f5b675da55844e8		00: La Factura electrónica SETP-990000975, ha sido autorizada.

FACTURA_VENTA		11/09/2020 6:58:33 p. m.		SETP990000976		3c54fd23115f54fad5c85abbfe368ac4faf70f9afa8b1dee97210b99728e640090ae326d4722becd896f55891d6573b3		00: La Factura electrónica SETP-990000976, ha sido autorizada.

FACTURA_VENTA		11/09/2020 7:03:13 p. m.		SETP990000977		391b04ddf0b3dbdcb200d2718c760fe453ef41fa9a7ef772fb751e6020bd277e9d2473d901fe8df838c7620a973bf562		00: La Factura electrónica SETP-990000977, ha sido autorizada.

FACTURA_VENTA		11/09/2020 7:09:07 p. m.		SETP990000978		cbdbbed0f39cccbfdbf5a467c076ac2b30857ef16ec0eb7056764a5fe1307a40f340ce0705c965947e1b7da323f8d38b		00: La Factura electrónica SETP-990000978, ha sido autorizada.

NOTA_CREDITO		11/09/2020 7:13:32 p. m.		SETP990000979		ee893e52f9f2bee770d1a017897851a11c0d2959b69278185d318bd0a791dfe32c55635278b0726b7955633189ed9b14		0: 

FACTURA_VENTA		11/09/2020 7:13:50 p. m.		SETP990000980		27032914dcf25c06ca1acda182f0ede9dfa9d57a992823ecc7453c32ad6eb8a4f6d10d162a5bc873444a8e73dacf6395		00: La Factura electrónica SETP-990000980, ha sido autorizada.

NOTA_CREDITO		11/09/2020 7:15:43 p. m.		SETP990000981		04f31b42d992615d9eeea9c86acf63e3c1af07325c1a6bede6a73c585db26f1a65ea19f7751e19cc7ffb99b0c2593890		0: 
