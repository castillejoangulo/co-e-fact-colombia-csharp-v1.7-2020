using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

using eFacturacionColombia_V2.Documentos;
using eFacturacionColombia_V2.Firma;
using eFacturacionColombia_V2.ServiciosWeb;
using eFacturacionColombia_V2.ServiciosWeb.DianServices;
using eFacturacionColombia_V2.Tipos.Standard;
using eFacturacionColombia_V2.Util;
using eFacturacionColombia_V2.Pdf;

namespace eFacturacionColombia_V2.Tests
{
    class Program
    {
        public static string ARCHIVO_CONSECUTIVO = "consecutivo.txt";
        public static string ARCHIVO_HISTORIAL = "historial.txt";

        #region Contribuyente, Software, Rango, etc.

        public static bool AMBIENTE_PRUEBAS = true;
        public static AmbienteServicio AMBIENTE_SERVICIO = AMBIENTE_PRUEBAS ? AmbienteServicio.PRUEBAS : AmbienteServicio.PRODUCCION;
        public static AmbienteDestino AMBIENTE_DESTINO = AMBIENTE_PRUEBAS ? AmbienteDestino.PRUEBAS : AmbienteDestino.PRODUCCION;

        public static string EMISOR_NIT = "24480134";
        public static string EMISOR_NOMBRE = "LUICIA AMPARO HERNANDEZ CARDENAS";
        public static DireccionFisica EMISOR_DIRECCION_FISICA = new DireccionFisica
        {
            Departamento = Departamento.QUINDIO,
            Municipio = Municipio.QUINDIO_ARMENIA,
            Linea = "CR 15 11 62",
            Pais = Pais.COLOMBIA
        };
        public static TipoContribuyente EMISOR_TIPO_CONTRIBUYENTE = TipoContribuyente.PERSONA_NATURAL;
        public static RegimenFiscal EMISOR_REGIMEN_FISCAL = RegimenFiscal.RESPONSABLE_DE_IVA;
        public static ResponsabilidadFiscal EMISOR_RESPONSABILIDAD_FISCAL = ResponsabilidadFiscal.AGENTE_RETENCION_IVA;
        public static string EMISOR_MATRICULA_MERCANTIL = "00000-00";  // https://www.rues.org.co/RM
        public static string EMISOR_CORREO_ELECTRONICO = "asistenciacontable123@hotmail.com";
        public static string EMISOR_NUMERO_TELEFONICO = "+57 6 745 5309";

        public static string SOFTWARE_IDENTIFICADOR = "4b11b40d-68bc-48ff-a08d-d9a6ff6ea9aa";
        public static string SOFTWARE_PIN = "12345";

        public static string IDENTIFICADOR_SET_PRUEBAS = "1faf3c36-b57b-4158-be9f-647182b15386";
        public static bool USAR_SET_PUEBAS = true;

        public static long RANGO_NUMERO_RESOLUCION = 18760000001;
        public static DateTime RANGO_FECHA_RESOLUCION = new DateTime(0001, 01, 01);
        public static string RANGO_PREFIJO = "SETP";
        public static long RANGO_DESDE = 990000000;
        public static long RANGO_HASTA = 995000000;
        public static string RANGO_CLAVE_TECNICA = "fc8eac422eba16e22ffd8c6f94b3f40a6e38162c";
        public static DateTime RANGO_VIGENCIA_DESDE = new DateTime(2019, 01, 19);
        public static DateTime RANGO_VIGENCIA_HASTA = new DateTime(2030, 01, 19);

        public static string RUTA_CERTIFICADO = "C:\\TEXMODA.p12";
        public static string CLAVE_CERTIFICADO = "W7NRAE3";

        #endregion

        static void Main(string[] args)
        {
            // Usar el protocolo TLS 1.2
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

//            TestConsultaRangos();

           TestFacturaEstandar();

//            TestFacturaExportacion();

            Console.ReadLine();
        }


        static void TestConsultaRangos()
        {
            Console.WriteLine();
            Console.WriteLine("Consultando rangos de numeración...");

            var cliente = new ClienteServicioDian
            {
                Ambiente = AmbienteServicio.PRODUCCION,
                // cargando certificado a partir de byte[] y contraseña
                Certificado = new X509Certificate2(File.ReadAllBytes(RUTA_CERTIFICADO), CLAVE_CERTIFICADO)
                // RutaCertificado = RUTA_CERTIFICADO,
                // ClaveCertificado = CLAVE_CERTIFICADO
            };

            var response = cliente.ObtenerRangosNumeracion(EMISOR_NIT, SOFTWARE_IDENTIFICADOR);

            Console.WriteLine("({0}) {1}", response.OperationCode, response.OperationDescription);

            if (response.ResponseList != null)
            {
                foreach (var range in response.ResponseList)
                {
                    Console.WriteLine("{0}   {1}   {2}   {3}   {4}   {5}   {6}   {7}", range.ResolutionNumber, range.ResolutionDate,
                        range.Prefix, range.FromNumber, range.ToNumber, range.TechnicalKey, range.ValidDateFrom, range.ValidDateTo);
                }
            }
        }
				
        static void TestFacturaEstandar()
        {
            Console.WriteLine();
            Console.WriteLine("Creando factura de venta...");

            var consecutivo = new ArchivoConsecutivo(ARCHIVO_CONSECUTIVO);
            string numero = RANGO_PREFIJO + consecutivo.ObtenerSiguiente();

            var fecha = DateTimeHelper.GetColombianDate();

            var vencimiento = fecha.AddMonths(1);

            var extensionDian = new ExtensionDian
            {
                RangoNumeracion = new RangoNumeracion
                {
                    NumeroResolucion = RANGO_NUMERO_RESOLUCION,
                    FechaResolucion = RANGO_FECHA_RESOLUCION,
                    Prefijo = RANGO_PREFIJO,
                    Desde = RANGO_DESDE,
                    Hasta = RANGO_HASTA,
                    VigenciaDesde = RANGO_VIGENCIA_DESDE,
                    VigenciaHasta = RANGO_VIGENCIA_HASTA
                },
                PaisOrigen = Pais.COLOMBIA,
                SoftwareProveedorNit = EMISOR_NIT,
                SoftwareIdentificador = SOFTWARE_IDENTIFICADOR,
                SoftwarePin = SOFTWARE_PIN
            };

            var emisor = new Emisor
            {
                Nit = EMISOR_NIT,
                Nombre = EMISOR_NOMBRE,
                DireccionFisica = EMISOR_DIRECCION_FISICA,
                TipoContribuyente = EMISOR_TIPO_CONTRIBUYENTE,
                RegimenFiscal = EMISOR_REGIMEN_FISCAL,
                ResponsabilidadFiscal = EMISOR_RESPONSABILIDAD_FISCAL,
                PrefijoRangoNumeracion = RANGO_PREFIJO,
                MatriculaMercantil = EMISOR_MATRICULA_MERCANTIL,
                CorreoElectronico = EMISOR_CORREO_ELECTRONICO,
                NumeroTelefonico = EMISOR_NUMERO_TELEFONICO
            };

            var adquiriente = new Adquiriente
            {
                TipoIdentificacion = TipoIdentificacion.CEDULA_CIUDADANIA,
                Identificacion = "89008003",
                Nombre = "ALEXANDER OBANDO LONDOÑO",
                DireccionFisica = new DireccionFisica
                {
                    Departamento = Departamento.QUINDIO,
                    Municipio = Municipio.QUINDIO_ARMENIA,
                    Linea = "LIMONAR MZ 6 CS 3 ET 1",
                    Pais = Pais.COLOMBIA
                },
                TipoContribuyente = TipoContribuyente.PERSONA_NATURAL,
                RegimenFiscal = RegimenFiscal.NO_RESPONSABLE_DE_IVA,
                ResponsabilidadFiscal = ResponsabilidadFiscal.NO_RESPONSABLE,
                MatriculaMercantil = "00000-00",
                CorreoElectronico = "alexander_obando@hotmail.com",
                NumeroTelefonico = "+57 310 389 1693"
            };

            var linea1 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Base para TV",
                PrecioUnitario = 300000M,
                CostoTotal = 250000M,
                Descuentos =
                {
                    new Descuento
                    {
                        Tipo = TipoDescuento.OTROS,
                        Razon = "Discount",
                        Porcentaje = 6M,
                        Monto = 50000M,
                        Base = 300000M
                    }
                },
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.IVA,
                        Porcentaje = 19M,
                        Importe = 47500M,
                        BaseImponible = 250000M
                    }
                }
            };

            var linea2 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Antena (regalo)",
                PrecioUnitario = 0M,
                PrecioUnitarioReal = 100000M,
                CostoTotal = 0M,
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.IVA,
                        Porcentaje = 19M,
                        Importe = 19000M,
                        BaseImponible = 100000M
                    }
                }
            };

            var linea3 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "TV",
                PrecioUnitario = 1400000M,
                CostoTotal = 1410000M,
                Cargos =
                {
                    new Cargo
                    {
                        Razon = "Cargo",
                        Porcentaje = 10M,
                        Monto = 10000,
                    }
                },
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.IVA,
                        Porcentaje = 19M,
                        Importe = 267900M,
                        BaseImponible = 1410000M
                    }
                }
            };

            var linea4 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Servicio (excluido)",
                PrecioUnitario = 20000M,
                CostoTotal = 20000M       
            };

            var linea5 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Acarreo",
                PrecioUnitario = 40000M,
                CostoTotal = 40000M,
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.IVA,
                        Porcentaje = 19M,
                        Importe = 7600M,
                        BaseImponible = 40000M
                    }
                }
            };

            var linea6 = new Linea
            {
                Cantidad = 2M,
                Descripcion = "Bolsas",
                PrecioUnitario = 0M,
                PrecioUnitarioReal = 200M,
                CostoTotal = 0M,
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.BOLSAS,
                        PorUnidad = 30M,
                        Importe = 60M
                    }
                }
            };

            var impuesto1 = new ResumenImpuesto
            {
                Tipo = TipoImpuesto.IVA,
                Importe = 342000M,
                Detalles =
                {
                    new Impuesto
                    {
                        Porcentaje = 19M,
                        Importe = 342000,
                        BaseImponible = 1800000M
                    }
                }
            };

            var impuesto2 = new ResumenImpuesto
            {
                Tipo = TipoImpuesto.IC,
                Importe = 0M
            };

            var impuesto3 = new ResumenImpuesto
            {
                Tipo = TipoImpuesto.ICA,
                Importe = 0M
            };

            var impuesto4 = new ResumenImpuesto
            {
                Tipo = TipoImpuesto.BOLSAS,
                Importe = 60M,
                Detalles =
                {
                    new Impuesto
                    {
                        PorUnidad = 30M,
                        Importe = 60M
                    }
                }
            };

            var descuento1 = new Descuento
            {
                Tipo = TipoDescuento.OTROS,
                Razon = "Descuento cliente frecuente",
                Porcentaje = 2.5M,
                Monto = 10000M,
                Base = 1720000M
            };

            var descuento2 = new Descuento
            {
                Tipo = TipoDescuento.OTROS,
                Razon = "Descuento por IVA asumido",
                Porcentaje = 2.5M,
                Monto = 19000M,
                Base = 1720000M
            };

            var descuento3 = new Descuento
            {
                Tipo = TipoDescuento.TEMPORADA,
                Razon = "Descuento por temporada",
                Porcentaje = 2.5M,
                Monto = 30000M,
                Base = 1720000M
            };

            var cargo1 = new Cargo
            {
                Razon = "Cargo financiero pago 30d",
                Porcentaje = 2.5M,
                Monto = 15000M,
                Base = 1720000M
            };

            var cargo2 = new Cargo
            {
                Razon = "Cargo financiero estudio de crédito",
                Porcentaje = 2.5M,
                Monto = 5000M,
                Base = 1720000M
            };

            var detallePago = new DetallePago
            {
                Forma = FormaPago.CREDITO,
                Metodo = MetodoPago.CONSIGNACION_BANCARIA,
                Fecha = vencimiento
            };

            var totales = new Totales
            {
                ValorBruto = 1720000M,
                BaseImponible = 1800000M,
                ValorBrutoConImpuestos = 2062060M,
                Descuentos = 59000M,
                Cargos = 20000M,
                TotalPagable = 2023060M
            };

            var generador = new GeneradorFactura(AMBIENTE_DESTINO, TipoFactura.VENTA)
                .ConNumero(numero)
                .ConFecha(fecha)
                .ConVencimiento(vencimiento)
                .ConNota("Set de pruebas")
                .ConMoneda(Moneda.PESO_COLOMBIANO)
                .ConExtensionDian(extensionDian)
                .ConEmisor(emisor)
                .ConAdquiriente(adquiriente)
                .AgregarLinea(linea1)
                .AgregarLinea(linea2)
                .AgregarLinea(linea3)
                .AgregarLinea(linea4)
                .AgregarLinea(linea5)
                .AgregarLinea(linea6)
                .AgregarResumenImpuesto(impuesto1)
                .AgregarResumenImpuesto(impuesto2)
                .AgregarResumenImpuesto(impuesto3)
                .AgregarResumenImpuesto(impuesto4)
                .AgregarDescuento(descuento1)
                .AgregarDescuento(descuento2)
                .AgregarDescuento(descuento3)
                .AgregarCargo(cargo1)
                .AgregarCargo(cargo2)
                .ConTotales(totales)
                .ConDetallePago(detallePago)
                .AsignarCUFE(RANGO_CLAVE_TECNICA, SOFTWARE_PIN)
                ;

            var qrFactura = generador.GenerarTextoQR();

            var invoice = generador.Obtener();

            var xmlInvoice = invoice.SerializeXmlDocument();

            var stringInvoice = xmlInvoice.ToXmlString();

            string unsignedXml = "docs/" + numero + "-unsigned.xml";
            File.WriteAllText(unsignedXml, stringInvoice);

            Console.WriteLine("Firmando...");

            var firmaElectronica = new FirmaElectronica
            {
                RolFirmante = RolFirmante.EMISOR,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            fecha = DateTimeHelper.GetColombianDate();

            var signedBytes = firmaElectronica.FirmarFactura(stringInvoice, fecha);

            var signedXml = Encoding.UTF8.GetString(signedBytes);

            string xmlFile = "docs/" + numero + "-signed.xml";
            File.WriteAllBytes(xmlFile, signedBytes);



            Console.WriteLine("Emitiendo...");

            var clienteDian = new ClienteServicioDian
            {
                Ambiente = AMBIENTE_SERVICIO,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            DianResponse dianResponse = null;
            UploadDocumentResponse uploadResponse = null;
            try
            {
                if (AMBIENTE_PRUEBAS && USAR_SET_PUEBAS)
                {
                    var documentos = new List<byte[]>() { signedBytes };

                    uploadResponse = clienteDian.EnviarSetPruebas(documentos, IDENTIFICADOR_SET_PRUEBAS);

                    Console.WriteLine("ZipKey: {0}", uploadResponse.ZipKey);

                    if (uploadResponse.ErrorMessageList != null)
                    {
                        foreach (var itemList in uploadResponse.ErrorMessageList)
                        {
                            Console.WriteLine("[{0}] {1}", itemList.DocumentKey, itemList.ProcessedMessage);
                        }
                    }


                    Console.WriteLine("Consultando validaciones...");

                    dianResponse = clienteDian.ConsultarDocumentos(uploadResponse.ZipKey)[0];

                    string registro = string.Format("\r\nFACTURA_VENTA\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", invoice.IssueDate.Value,
                        invoice.ID.Value, invoice.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                    // Suponiendo que cuando appresponse no es null significa que la dian aceptó el documento

                    if (appResponse != "null")
                    {
                        var applicationResponse = ApplicationResponseType.DeserializeXmlString(appResponse);

                        var generadorContenedor = new GeneradorContenedor(AMBIENTE_DESTINO)
                            .ConDocumento(invoice) // Se pasa invoicetype, creditnotetype o debitnotetype según corresponda
                            .ConRespuesta(applicationResponse)
                            ;



                        var attachedDocument = generadorContenedor.Obtener();
                        var xmlAttachedDocument = System.Net.WebUtility.HtmlDecode(attachedDocument.SerializeXmlDocument().ToXmlString());
                        var bytesAttachedDocument = Encoding.UTF8.GetBytes(xmlAttachedDocument);
                        // var bytesAttachedDocument = GetBytes(xmlAttachedDocument);

                        File.WriteAllText("docs/" + numero + "-attacheddocument.xml", xmlAttachedDocument);
                        var zipAttachedDocument = ZipHelper.Compress(bytesAttachedDocument, "attached-document.xml");

                        // Este es el zip que se adjunta en el correo, que se envia al adquiriente, junto al pdf
                    }
                }
                else if (AMBIENTE_PRUEBAS)
                {
                    dianResponse = clienteDian.EnviarDocumento(signedBytes);

                    string registro = string.Format("\r\nFACTURA_VENTA\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", invoice.IssueDate.Value,
                        invoice.ID.Value, invoice.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }
                else
                {
                    dianResponse = clienteDian.EnviarDocumento(signedBytes);

                    string registro = string.Format("\r\nFACTURA_VENTA\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", invoice.IssueDate.Value,
                        invoice.ID.Value, invoice.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                
                return;
            }



            Console.WriteLine("Creando pdf...");

            try
            {
                var facturaPdf = new FacturaPdf(invoice, DocumentoPdf.TIPO_FACTURA_VENTA)
                {
                    RutaLogo = "logo.png",
                    TextoEncabezado = "Persona Jurídica \r\nRégimen Ordinario",
                    TextoConstancia = DocumentoPdf.TEXTO_CONSTANCIA_FACTURA_DEFAULT,
                    TextoQR = generador.GenerarTextoQR(),
                    TextoResolucion = DocumentoPdf.CrearTextoResolucion(RANGO_NUMERO_RESOLUCION,
                        RANGO_FECHA_RESOLUCION, RANGO_PREFIJO, RANGO_DESDE, RANGO_HASTA, RANGO_VIGENCIA_HASTA)
                };

                var bytesPdf = facturaPdf.Generar();

                File.WriteAllBytes("docs/" + numero + ".pdf", bytesPdf);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }



            // nota de crédito
            TestNotaCredito(invoice);
            TestAcuseRecibo(invoice);
        }

        static void TestFacturaExportacion()
        {
            Console.WriteLine();
            Console.WriteLine("Creando factura de exportación...");

            var consecutivo = new ArchivoConsecutivo(ARCHIVO_CONSECUTIVO);
            string numero = RANGO_PREFIJO + consecutivo.ObtenerSiguiente();

            var fecha = DateTimeHelper.GetColombianDate();

            var vencimiento = fecha.AddMonths(1);

            var extensionDian = new ExtensionDian
            {
                RangoNumeracion = new RangoNumeracion
                {
                    NumeroResolucion = RANGO_NUMERO_RESOLUCION,
                    FechaResolucion = RANGO_FECHA_RESOLUCION,
                    Prefijo = RANGO_PREFIJO,
                    Desde = RANGO_DESDE,
                    Hasta = RANGO_HASTA,
                    VigenciaDesde = RANGO_VIGENCIA_DESDE,
                    VigenciaHasta = RANGO_VIGENCIA_HASTA
                },
                PaisOrigen = Pais.COLOMBIA,
                SoftwareProveedorNit = EMISOR_NIT,
                SoftwareIdentificador = SOFTWARE_IDENTIFICADOR,
                SoftwarePin = SOFTWARE_PIN
            };

            var emisor = new Emisor
            {
                Nit = EMISOR_NIT,
                Nombre = EMISOR_NOMBRE,
                DireccionFisica = EMISOR_DIRECCION_FISICA,
                TipoContribuyente = EMISOR_TIPO_CONTRIBUYENTE,
                RegimenFiscal = EMISOR_REGIMEN_FISCAL,
                ResponsabilidadFiscal = EMISOR_RESPONSABILIDAD_FISCAL,
                PrefijoRangoNumeracion = RANGO_PREFIJO,
                MatriculaMercantil = EMISOR_MATRICULA_MERCANTIL,
                CorreoElectronico = EMISOR_CORREO_ELECTRONICO,
                NumeroTelefonico = EMISOR_NUMERO_TELEFONICO
            };

            var adquiriente = new Adquiriente
            {
                TipoIdentificacion = TipoIdentificacion.DOCUMENTO_IDENTIFICACION_EXTRANJERO,
                Identificacion = "FR132123124",
                Nombre = "ADQUIRIENTE INTERNACIONAL",
                DireccionFisica = new DireccionFisica
                {
                    Departamento = new Departamento
                    {
                        Codigo = "99",
                        Nombre = "PRUEBA PRUEBA PRUEBA"
                    },
                    Municipio = new Municipio
                    {
                        Codigo = "999999",
                        Nombre = "PRUEBA PRUEBA"
                    },
                    Linea = "34 Rue du general leclercq",
                    Pais = new Pais
                    {
                        Codigo = "FR",
                        Nombre = "PRUEBA"
                    }
                },
                TipoContribuyente = TipoContribuyente.PERSONA_JURIDICA,
                RegimenFiscal = RegimenFiscal.NO_RESPONSABLE_DE_IVA,
                ResponsabilidadFiscal = ResponsabilidadFiscal.REGIMEN_SIMPLE_TRIBUTACION,
                MatriculaMercantil = "00000",
                CorreoElectronico = "tests@miguel-huertas.net",
                NumeroTelefonico = "+000 00000000"
            };

            var linea1 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Base para TV",
                PrecioUnitario = 300000M,
                CostoTotal = 250000M,
                Descuentos =
                {
                    new Descuento
                    {
                        Tipo = TipoDescuento.OTROS,
                        Razon = "Discount",
                        Porcentaje = 6M,
                        Monto = 50000M,
                        Base = 300000M
                    }
                }
            };

            var linea2 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Antena (regalo)",
                PrecioUnitario = 0M,
                PrecioUnitarioReal = 100000M,
                CostoTotal = 0M
            };

            var linea3 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "TV",
                PrecioUnitario = 1400000M,
                CostoTotal = 1410000M,
                Cargos =
                {
                    new Cargo
                    {
                        Razon = "Cargo",
                        Porcentaje = 10M,
                        Monto = 10000,
                    }
                }
            };

            var linea4 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Servicio (excluido)",
                PrecioUnitario = 20000M,
                CostoTotal = 20000M
            };

            var linea5 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Acarreo",
                PrecioUnitario = 40000M,
                CostoTotal = 40000M
            };

            var linea6 = new Linea
            {
                Cantidad = 2M,
                Descripcion = "Bolsas",
                PrecioUnitario = 0M,
                PrecioUnitarioReal = 200M,
                CostoTotal = 0M,
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.BOLSAS,
                        PorUnidad = 30M,
                        Importe = 60M
                    }
                }
            };

            var impuesto1 = new ResumenImpuesto
            {
                Tipo = TipoImpuesto.BOLSAS,
                Importe = 60M,
                Detalles =
                {
                    new Impuesto
                    {
                        PorUnidad = 30M,
                        Importe = 60M
                    }
                }
            };
           
            var descuento1 = new Descuento
            {
                Tipo = TipoDescuento.OTROS,
                Razon = "Descuento cliente frecuente",
                Porcentaje = 10M,
                Monto = 10000M,
                Base = 1720000M
            };

            var descuento2 = new Descuento
            {
                Tipo = TipoDescuento.TEMPORADA,
                Razon = "Descuento por temporada",
                Porcentaje = 10M,
                Monto = 30000M,
                Base = 1720000M
            };

            var cargo1 = new Cargo
            {
                Razon = "Cargo financiero pago 30d",
                Porcentaje = 10M,
                Monto = 15000M,
                Base = 1720000M
            };

            var cargo2 = new Cargo
            {
                Razon = "Cargo financiero estudio de crédito",
                Porcentaje = 10M,
                Monto = 5000M,
                Base = 1720000M
            };

            var tasaCambio = new TasaCambio
            {
                Origen = Moneda.EURO,
                Destino = Moneda.PESO_COLOMBIANO,
                Cambio = 3416.50M,
                Fecha = fecha
            };

            var detallePago = new DetallePago
            {
                Forma = FormaPago.CONTADO,
                Metodo = MetodoPago.TARJETA_DEBITO,
                Fecha = vencimiento
            };

            var terminosEntrega = new TerminosEntrega
            {
                TerminosEspeciales = "Texto Libre descripcion de las condiciones de entrega",
                Condicion = CondicionEntrega.COSTO_FLETE_Y_SEGURO
            };

            var totales = new Totales
            {
                ValorBruto = 1720000M,
                BaseImponible = 0M,
                ValorBrutoConImpuestos = 1720060M,
                Descuentos = 40000M,
                Cargos = 20000M,
                TotalPagable = 1700060M
            };

            var generador = new GeneradorFactura(AMBIENTE_DESTINO, TipoFactura.EXPORTACION)
                .ConNumero(numero)
                .ConFecha(fecha)
                .ConVencimiento(vencimiento)
                .ConNota("Set de pruebas")
                .ConMoneda(Moneda.EURO)
                .ConExtensionDian(extensionDian)
                .ConEmisor(emisor)
                .ConAdquiriente(adquiriente)
                .AgregarLinea(linea1)
                .AgregarLinea(linea2)
                .AgregarLinea(linea3)
                .AgregarLinea(linea4)
                .AgregarLinea(linea5)
                .AgregarLinea(linea6)
                .AgregarResumenImpuesto(impuesto1)
                .AgregarDescuento(descuento1)
                .AgregarDescuento(descuento2)
                .AgregarCargo(cargo1)
                .AgregarCargo(cargo2)
                .ConTotales(totales)
                .ConTasaCambio(tasaCambio) //+exp
                .ConDetallePago(detallePago)
                .ConTerminosEntrega(terminosEntrega) // +exp
                .AsignarCUFE(RANGO_CLAVE_TECNICA, SOFTWARE_PIN)
                ;

            string qrInvoice = generador.GenerarTextoQR();

            var invoice = generador.Obtener();

            var xmlInvoice = invoice.SerializeXmlDocument();

            var stringInvoice = xmlInvoice.ToXmlString();

            string unsignedXml = "docs/" + numero + "-unsigned.xml";
            File.WriteAllText(unsignedXml, stringInvoice);



            Console.WriteLine("Firmando...");

            var firmaElectronica = new FirmaElectronica
            {
                RolFirmante = RolFirmante.EMISOR,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            fecha = DateTimeHelper.GetColombianDate();

            var signedBytes = firmaElectronica.FirmarFactura(stringInvoice, fecha);

            var signedXml = Encoding.UTF8.GetString(signedBytes);

            string xmlFile = "docs/" + numero + "-signed.xml";
            File.WriteAllBytes(xmlFile, signedBytes);



            Console.WriteLine("Emitiendo...");

            var clienteDian = new ClienteServicioDian
            {
                Ambiente = AMBIENTE_SERVICIO,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            DianResponse dianResponse = null;
            UploadDocumentResponse uploadResponse = null;
            try
            {
                if (AMBIENTE_PRUEBAS && USAR_SET_PUEBAS)
                {
                    var documentos = new List<byte[]>() { signedBytes };

                    uploadResponse = clienteDian.EnviarSetPruebas(documentos, IDENTIFICADOR_SET_PRUEBAS);

                    Console.WriteLine("ZipKey: {0}", uploadResponse.ZipKey);

                    if (uploadResponse.ErrorMessageList != null)
                    {
                        foreach (var itemList in uploadResponse.ErrorMessageList)
                        {
                            Console.WriteLine("[{0}] {1}", itemList.DocumentKey, itemList.ProcessedMessage);
                        }
                    }


                    Console.WriteLine("Consultando validaciones...");

                    dianResponse = clienteDian.ConsultarDocumentos(uploadResponse.ZipKey)[0];

                    string registro = string.Format("\r\nFACTURA_EXPORTACION\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", invoice.IssueDate.Value,
                        invoice.ID.Value, invoice.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }
                else if (AMBIENTE_PRUEBAS)
                {
                    dianResponse = clienteDian.EnviarDocumento(signedBytes);

                    string registro = string.Format("\r\nFACTURA_EXPORTACION\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", invoice.IssueDate.Value,
                        invoice.ID.Value, invoice.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }
                else
                {
                    dianResponse = clienteDian.EnviarDocumento(signedBytes);

                    string registro = string.Format("\r\nFACTURA_EXPORTACION\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", invoice.IssueDate.Value,
                        invoice.ID.Value, invoice.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return;
            }



            Console.WriteLine("Creando pdf...");

            try
            {
                var facturaPdf = new FacturaPdf(invoice, DocumentoPdf.TIPO_FACTURA_EXPORTACION)
                {
                    RutaLogo = "logo.png",
                    TextoEncabezado = "Persona Jurídica \r\nRégimen Ordinario",
                    TextoConstancia = DocumentoPdf.TEXTO_CONSTANCIA_FACTURA_DEFAULT,
                    TextoQR = generador.GenerarTextoQR(),
                    TextoResolucion = DocumentoPdf.CrearTextoResolucion(RANGO_NUMERO_RESOLUCION,
                        RANGO_FECHA_RESOLUCION, RANGO_PREFIJO, RANGO_DESDE, RANGO_HASTA, RANGO_VIGENCIA_HASTA)
                };

                var bytesPdf = facturaPdf.Generar();

                File.WriteAllBytes("docs/" + numero + ".pdf", bytesPdf);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }



            // nota de débito
//            TestNotaDebito(invoice);
        }

        static void TestNotaCredito(InvoiceType invoice)
        {
            Console.WriteLine();
            Console.WriteLine("Creando nota de crédito...");

            var consecutivo = new ArchivoConsecutivo(ARCHIVO_CONSECUTIVO);
            string numero = RANGO_PREFIJO + consecutivo.ObtenerSiguiente();

            var fecha = DateTimeHelper.GetColombianDate();

            var extensionDian = new ExtensionDian
            {
                RangoNumeracion = new RangoNumeracion
                {
                    NumeroResolucion = RANGO_NUMERO_RESOLUCION,
                    FechaResolucion = RANGO_FECHA_RESOLUCION,
                    Prefijo = RANGO_PREFIJO,
                    Desde = RANGO_DESDE,
                    Hasta = RANGO_HASTA,
                    VigenciaDesde = RANGO_VIGENCIA_DESDE,
                    VigenciaHasta = RANGO_VIGENCIA_HASTA
                },
                PaisOrigen = Pais.COLOMBIA,
                SoftwareProveedorNit = EMISOR_NIT,
                SoftwareIdentificador = SOFTWARE_IDENTIFICADOR,
                SoftwarePin = SOFTWARE_PIN
            };

            var emisor = new Emisor
            {
                Nit = EMISOR_NIT,
                Nombre = EMISOR_NOMBRE,
                DireccionFisica = EMISOR_DIRECCION_FISICA,
                TipoContribuyente = EMISOR_TIPO_CONTRIBUYENTE,
                RegimenFiscal = EMISOR_REGIMEN_FISCAL,
                ResponsabilidadFiscal = EMISOR_RESPONSABILIDAD_FISCAL,
                PrefijoRangoNumeracion = RANGO_PREFIJO,
                MatriculaMercantil = EMISOR_MATRICULA_MERCANTIL,
                CorreoElectronico = EMISOR_CORREO_ELECTRONICO,
                NumeroTelefonico = EMISOR_NUMERO_TELEFONICO
            };

            var adquiriente = new Adquiriente
            {
                TipoIdentificacion = TipoIdentificacion.CEDULA_CIUDADANIA,
                Identificacion = "89008003",
                Nombre = "ALEXANDER OBANDO LONDOÑO",
                DireccionFisica = new DireccionFisica
                {
                    Departamento = Departamento.QUINDIO,
                    Municipio = Municipio.QUINDIO_ARMENIA,
                    Linea = "LIMONAR MZ 6 CS 3 ET 1",
                    Pais = Pais.COLOMBIA
                },
                TipoContribuyente = TipoContribuyente.PERSONA_NATURAL,
                RegimenFiscal = RegimenFiscal.NO_RESPONSABLE_DE_IVA,
                ResponsabilidadFiscal = ResponsabilidadFiscal.NO_RESPONSABLE,
                MatriculaMercantil = "00000-00",
                CorreoElectronico = "alexander_obando@hotmail.com",
                NumeroTelefonico = "+57 310 389 1693"
            };

            var linea1 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Base para TV",
                PrecioUnitario = 300000M,
                CostoTotal = 250000M,
                Descuentos =
                {
                    new Descuento
                    {
                        Tipo = TipoDescuento.OTROS,
                        Razon = "Discount",
                        Porcentaje = 6M,
                        Monto = 50000M,
                        Base = 300000M
                    }
                },
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.IVA,
                        Porcentaje = 19M,
                        Importe = 47500M,
                        BaseImponible = 250000M
                    }
                }
            };

            var linea2 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Antena (regalo)",
                PrecioUnitario = 0M,
                PrecioUnitarioReal = 100000M,
                CostoTotal = 0M,
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.IVA,
                        Porcentaje = 19M,
                        Importe = 19000M,
                        BaseImponible = 100000M
                    }
                }
            };

            var linea3 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "TV",
                PrecioUnitario = 1400000M,
                CostoTotal = 1410000M,
                Cargos =
                {
                    new Cargo
                    {
                        Razon = "Cargo",
                        Porcentaje = 10M,
                        Monto = 10000,
                    }
                },
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.IVA,
                        Porcentaje = 19M,
                        Importe = 267900M,
                        BaseImponible = 1410000M
                    }
                }
            };

            var linea4 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Servicio (excluido)",
                PrecioUnitario = 20000M,
                CostoTotal = 20000M
            };

            var linea5 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Acarreo",
                PrecioUnitario = 40000M,
                CostoTotal = 40000M,
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.IVA,
                        Porcentaje = 19M,
                        Importe = 7600M,
                        BaseImponible = 40000M
                    }
                }
            };

            var linea6 = new Linea
            {
                Cantidad = 2M,
                Descripcion = "Bolsas",
                PrecioUnitario = 0M,
                PrecioUnitarioReal = 200M,
                CostoTotal = 0M,
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.BOLSAS,
                        PorUnidad = 30M,
                        Importe = 60M
                    }
                }
            };

            var impuesto1 = new ResumenImpuesto
            {
                Tipo = TipoImpuesto.IVA,
                Importe = 342000M,
                Detalles =
                {
                    new Impuesto
                    {
                        Porcentaje = 19M,
                        Importe = 342000,
                        BaseImponible = 1800000M
                    }
                }
            };

            var impuesto2 = new ResumenImpuesto
            {
                Tipo = TipoImpuesto.IC,
                Importe = 0M
            };

            var impuesto3 = new ResumenImpuesto
            {
                Tipo = TipoImpuesto.ICA,
                Importe = 0M
            };

            var impuesto4 = new ResumenImpuesto
            {
                Tipo = TipoImpuesto.BOLSAS,
                Importe = 60M,
                Detalles =
                {
                    new Impuesto
                    {
                        PorUnidad = 30M,
                        Importe = 60M
                    }
                }
            };

            var descuento1 = new Descuento
            {
                Tipo = TipoDescuento.OTROS,
                Razon = "Descuento cliente frecuente",
                Porcentaje = 2.5M,
                Monto = 10000M,
                Base = 1720000M
            };

            var descuento2 = new Descuento
            {
                Tipo = TipoDescuento.OTROS,
                Razon = "Descuento por IVA asumido",
                Porcentaje = 2.5M,
                Monto = 19000M,
                Base = 1720000M
            };

            var descuento3 = new Descuento
            {
                Tipo = TipoDescuento.TEMPORADA,
                Razon = "Descuento por temporada",
                Porcentaje = 2.5M,
                Monto = 30000M,
                Base = 1720000M
            };

            var cargo1 = new Cargo
            {
                Razon = "Cargo financiero pago 30d",
                Porcentaje = 2.5M,
                Monto = 15000M,
                Base = 1720000M
            };

            var cargo2 = new Cargo
            {
                Razon = "Cargo financiero estudio de crédito",
                Porcentaje = 2.5M,
                Monto = 5000M,
                Base = 1720000M
            };

            var detallePago = new DetallePago
            {
                Forma = FormaPago.CREDITO,
                Metodo = MetodoPago.CONSIGNACION_BANCARIA,
                Fecha = invoice.PaymentMeans[0].PaymentDueDate.Value
            };

            var totales = new Totales
            {
                ValorBruto = 1720000M,
                BaseImponible = 1800000M,
                ValorBrutoConImpuestos = 2062060M,
                Descuentos = 59000M,
                Cargos = 20000M,
                TotalPagable = 2023060M
            };

            var concepto = ConceptoNotaCredito.ANULACION_TOTAL;

            var referencia = new ReferenciaDocumento
            {
                Numero = invoice.ID.Value,
                Cufe = invoice.UUID.Value,
                AlgoritmoCufe = new AlgoritmoSeguridadUUID { Codigo = invoice.UUID.schemeName },
                TipoDocumento = new TipoDocumento { Codigo = invoice.InvoiceTypeCode.Value },
                Fecha = invoice.IssueDate.Value
            };

            var generador = new GeneradorNotaCredito(AMBIENTE_DESTINO, concepto, referencia)
                .ConNumero(numero)
                .ConFecha(fecha)
                .ConNota("Set de pruebas")
                .ConMoneda(Moneda.PESO_COLOMBIANO)
                .ConExtensionDian(extensionDian)
                .ConEmisor(emisor)
                .ConAdquiriente(adquiriente)
                .AgregarLinea(linea1)
                .AgregarLinea(linea2)
                .AgregarLinea(linea3)
                .AgregarLinea(linea4)
                .AgregarLinea(linea5)
                .AgregarLinea(linea6)
                .AgregarResumenImpuesto(impuesto1)
                .AgregarResumenImpuesto(impuesto2)
                .AgregarResumenImpuesto(impuesto3)
                .AgregarResumenImpuesto(impuesto4)
                .AgregarDescuento(descuento1)
                .AgregarDescuento(descuento2)
                .AgregarDescuento(descuento3)
                .AgregarCargo(cargo1)
                .AgregarCargo(cargo2)
                .ConTotales(totales)
                .ConDetallePago(detallePago)
                .AsignarCUDE(SOFTWARE_PIN)
                ;

            var creditNote = generador.Obtener();

            var xmlCreditNote = creditNote.SerializeXmlDocument();

            var stringCreditNote = xmlCreditNote.ToXmlString();

            string unsignedXml = "docs/" + numero + "-unsigned.xml";
            File.WriteAllText(unsignedXml, stringCreditNote);



            Console.WriteLine("Firmando...");

            var firmaElectronica = new FirmaElectronica
            {
                RolFirmante = RolFirmante.EMISOR,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            fecha = DateTimeHelper.GetColombianDate();

            var signedBytes = firmaElectronica.FirmarNotaCredito(stringCreditNote, fecha);

            var signedXml = Encoding.UTF8.GetString(signedBytes);

            string xmlFile = "docs/" + numero + "-signed.xml";
            File.WriteAllBytes(xmlFile, signedBytes);



            Console.WriteLine("Emitiendo...");

            var clienteDian = new ClienteServicioDian
            {
                Ambiente = AMBIENTE_SERVICIO,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            DianResponse dianResponse = null;
            UploadDocumentResponse uploadResponse = null;
            try
            {
                if (AMBIENTE_PRUEBAS && USAR_SET_PUEBAS)
                {
                    var documentos = new List<byte[]>() { signedBytes };

                    uploadResponse = clienteDian.EnviarSetPruebas(documentos, IDENTIFICADOR_SET_PRUEBAS);

                    Console.WriteLine("ZipKey: {0}", uploadResponse.ZipKey);

                    if (uploadResponse.ErrorMessageList != null)
                    {
                        foreach (var itemList in uploadResponse.ErrorMessageList)
                        {
                            Console.WriteLine("[{0}] {1}", itemList.DocumentKey, itemList.ProcessedMessage);
                        }
                    }


                    Console.WriteLine("Consultando validaciones...");

                    dianResponse = clienteDian.ConsultarDocumentos(uploadResponse.ZipKey)[0];

                    string registro = string.Format("\r\nNOTA_CREDITO\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", creditNote.IssueDate.Value,
                        creditNote.ID.Value, creditNote.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }
                else if (AMBIENTE_PRUEBAS)
                {
                    dianResponse = clienteDian.EnviarDocumento(signedBytes);

                    string registro = string.Format("\r\nNOTA_CREDITO\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", creditNote.IssueDate.Value,
                        creditNote.ID.Value, creditNote.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }
                else
                {
                    dianResponse = clienteDian.EnviarDocumento(signedBytes);

                    string registro = string.Format("\r\nNOTA_CREDITO\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", creditNote.IssueDate.Value,
                        creditNote.ID.Value, creditNote.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return;
            }



            Console.WriteLine("Creando pdf...");

            try
            {
                var notaCreditoPdf = new NotaCreditoPdf(creditNote)
                {
                    RutaLogo = "logo.png",
                    TextoEncabezado = "Persona Jurídica \r\nRégimen Ordinario",
                    TextoQR = creditNote.UUID.Value,
                    TextoResolucion = DocumentoPdf.CrearTextoResolucion(RANGO_NUMERO_RESOLUCION,
                        RANGO_FECHA_RESOLUCION, RANGO_PREFIJO, RANGO_DESDE, RANGO_HASTA, RANGO_VIGENCIA_HASTA)
                };

                var bytesPdf = notaCreditoPdf.Generar();

                File.WriteAllBytes("docs/" + numero + ".pdf", bytesPdf);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void TestNotaDebito(InvoiceType invoice)
        {
            Console.WriteLine();
            Console.WriteLine("Creando nota de débito...");

            var consecutivo = new ArchivoConsecutivo(ARCHIVO_CONSECUTIVO);
            string numero = RANGO_PREFIJO + consecutivo.ObtenerSiguiente();

            var fecha = DateTimeHelper.GetColombianDate();

            var extensionDian = new ExtensionDian
            {
                RangoNumeracion = new RangoNumeracion
                {
                    NumeroResolucion = RANGO_NUMERO_RESOLUCION,
                    FechaResolucion = RANGO_FECHA_RESOLUCION,
                    Prefijo = RANGO_PREFIJO,
                    Desde = RANGO_DESDE,
                    Hasta = RANGO_HASTA,
                    VigenciaDesde = RANGO_VIGENCIA_DESDE,
                    VigenciaHasta = RANGO_VIGENCIA_HASTA
                },
                PaisOrigen = Pais.COLOMBIA,
                SoftwareProveedorNit = EMISOR_NIT,
                SoftwareIdentificador = SOFTWARE_IDENTIFICADOR,
                SoftwarePin = SOFTWARE_PIN
            };

            var emisor = new Emisor
            {
                Nit = EMISOR_NIT,
                Nombre = EMISOR_NOMBRE,
                DireccionFisica = EMISOR_DIRECCION_FISICA,
                TipoContribuyente = EMISOR_TIPO_CONTRIBUYENTE,
                RegimenFiscal = EMISOR_REGIMEN_FISCAL,
                ResponsabilidadFiscal = EMISOR_RESPONSABILIDAD_FISCAL,
                PrefijoRangoNumeracion = RANGO_PREFIJO,
                MatriculaMercantil = EMISOR_MATRICULA_MERCANTIL,
                CorreoElectronico = EMISOR_CORREO_ELECTRONICO,
                NumeroTelefonico = EMISOR_NUMERO_TELEFONICO
            };

            var adquiriente = new Adquiriente
            {
                TipoIdentificacion = TipoIdentificacion.DOCUMENTO_IDENTIFICACION_EXTRANJERO,
                Identificacion = "FR132123124",
                Nombre = "ADQUIRIENTE INTERNACIONAL",
                DireccionFisica = new DireccionFisica
                {
                    Departamento = new Departamento
                    {
                        Codigo = "59",
                        Nombre = "Hauts de France"
                    },
                    Municipio = new Municipio
                    {
                        Codigo = "59001",
                        Nombre = "LILLE"
                    },
                    Linea = "34 Rue du general leclercq",
                    Pais = new Pais
                    {
                        Codigo = "FR",
                        Nombre = "Francia"
                    }
                },
                TipoContribuyente = TipoContribuyente.PERSONA_JURIDICA,
                RegimenFiscal = RegimenFiscal.NO_RESPONSABLE_DE_IVA,
                ResponsabilidadFiscal = ResponsabilidadFiscal.REGIMEN_SIMPLE_TRIBUTACION,
                MatriculaMercantil = "00000",
                CorreoElectronico = "tests@miguel-huertas.net",
                NumeroTelefonico = "+000 00000000"
            };

            var linea1 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Base para TV",
                PrecioUnitario = 300000M,
                CostoTotal = 250000M,
                Descuentos =
                {
                    new Descuento
                    {
                        Tipo = TipoDescuento.OTROS,
                        Razon = "Discount",
                        Porcentaje = 6M,
                        Monto = 50000M,
                        Base = 300000M
                    }
                }
            };

            var linea2 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Antena (regalo)",
                PrecioUnitario = 0M,
                PrecioUnitarioReal = 100000M,
                CostoTotal = 0M
            };

            var linea3 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "TV",
                PrecioUnitario = 1400000M,
                CostoTotal = 1410000M,
                Cargos =
                {
                    new Cargo
                    {
                        Razon = "Cargo",
                        Porcentaje = 10M,
                        Monto = 10000,
                    }
                }
            };

            var linea4 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Servicio (excluido)",
                PrecioUnitario = 20000M,
                CostoTotal = 20000M
            };

            var linea5 = new Linea
            {
                Cantidad = 1M,
                Descripcion = "Acarreo",
                PrecioUnitario = 40000M,
                CostoTotal = 40000M
            };

            var linea6 = new Linea
            {
                Cantidad = 2M,
                Descripcion = "Bolsas",
                PrecioUnitario = 0M,
                PrecioUnitarioReal = 200M,
                CostoTotal = 0M,
                Impuestos =
                {
                    new Impuesto
                    {
                        Tipo = TipoImpuesto.BOLSAS,
                        PorUnidad = 30M,
                        Importe = 60M
                    }
                }
            };

            var impuesto1 = new ResumenImpuesto
            {
                Tipo = TipoImpuesto.BOLSAS,
                Importe = 60M,
                Detalles =
                {
                    new Impuesto
                    {
                        PorUnidad = 30M,
                        Importe = 60M
                    }
                }
            };

            var descuento1 = new Descuento
            {
                Tipo = TipoDescuento.OTROS,
                Razon = "Descuento cliente frecuente",
                Porcentaje = 10M,
                Monto = 10000M,
                Base = 1720000M
            };

            var descuento2 = new Descuento
            {
                Tipo = TipoDescuento.TEMPORADA,
                Razon = "Descuento por temporada",
                Porcentaje = 10M,
                Monto = 30000M,
                Base = 1720000M
            };

            var cargo1 = new Cargo
            {
                Razon = "Cargo financiero pago 30d",
                Porcentaje = 10M,
                Monto = 15000M,
                Base = 1720000M
            };

            var cargo2 = new Cargo
            {
                Razon = "Cargo financiero estudio de crédito",
                Porcentaje = 10M,
                Monto = 5000M,
                Base = 1720000M
            };

            var tasaCambio = new TasaCambio
            {
                Origen = Moneda.EURO,
                Destino = Moneda.PESO_COLOMBIANO,
                Cambio = 3416.50M,
                Fecha = fecha
            };

            var detallePago = new DetallePago
            {
                Forma = FormaPago.CONTADO,
                Metodo = MetodoPago.TARJETA_DEBITO,
                Fecha = invoice.PaymentMeans[0].PaymentDueDate.Value
            };

            var terminosEntrega = new TerminosEntrega
            {
                TerminosEspeciales = "Texto Libre descripcion de las condiciones de entrega",
                Condicion = CondicionEntrega.COSTO_FLETE_Y_SEGURO
            };

            var totales = new Totales
            {
                ValorBruto = 1720000M,
                BaseImponible = 0M,
                ValorBrutoConImpuestos = 1720060M,
                Descuentos = 40000M,
                Cargos = 20000M,
                TotalPagable = 1700060M
            };

            var concepto = ConceptoNotaDebito.OTROS;

            var referencia = new ReferenciaDocumento
            {
                Numero = invoice.ID.Value,
                Cufe = invoice.UUID.Value,
                AlgoritmoCufe = new AlgoritmoSeguridadUUID { Codigo = invoice.UUID.schemeName },
                TipoDocumento = new TipoDocumento { Codigo = invoice.InvoiceTypeCode.Value },
                Fecha = invoice.IssueDate.Value
            };

            var generador = new GeneradorNotaDebito(AMBIENTE_DESTINO, concepto, referencia)
                .ConNumero(numero)
                .ConFecha(fecha)
                .ConNota("Set de pruebas")
                .ConMoneda(Moneda.EURO)
                .ConExtensionDian(extensionDian)
                .ConEmisor(emisor)
                .ConAdquiriente(adquiriente)
                .AgregarLinea(linea1)
                .AgregarLinea(linea2)
                .AgregarLinea(linea3)
                .AgregarLinea(linea4)
                .AgregarLinea(linea5)
                .AgregarLinea(linea6)
                .AgregarResumenImpuesto(impuesto1)
                .AgregarDescuento(descuento1)
                .AgregarDescuento(descuento2)
                .AgregarCargo(cargo1)
                .AgregarCargo(cargo2)
                .ConTotales(totales)
                .ConTasaCambio(tasaCambio)
                .ConDetallePago(detallePago)
                .ConTerminosEntrega(terminosEntrega)
                .AsignarCUDE(SOFTWARE_PIN)
                ;

            var debitNote = generador.Obtener();

            var xmlDebitNote = debitNote.SerializeXmlDocument();

            var stringDebitNote = xmlDebitNote.ToXmlString();

            string unsignedXml = "docs/" + numero + "-unsigned.xml";
            File.WriteAllText(unsignedXml, stringDebitNote);


            Console.WriteLine("Firmando...");

            var firmaElectronica = new FirmaElectronica
            {
                RolFirmante = RolFirmante.EMISOR,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            fecha = DateTimeHelper.GetColombianDate();

            var signedBytes = firmaElectronica.FirmarNotaDebito(stringDebitNote, fecha);

            var signedXml = Encoding.UTF8.GetString(signedBytes);

            string xmlFile = "docs/" + numero + "-signed.xml";
            File.WriteAllBytes(xmlFile, signedBytes);



            Console.WriteLine("Emitiendo...");

            var clienteDian = new ClienteServicioDian
            {
                Ambiente = AMBIENTE_SERVICIO,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            DianResponse dianResponse = null;
            UploadDocumentResponse uploadResponse = null;
            try
            {
                if (AMBIENTE_PRUEBAS && USAR_SET_PUEBAS)
                {
                    var documentos = new List<byte[]>() { signedBytes };

                    uploadResponse = clienteDian.EnviarSetPruebas(documentos, IDENTIFICADOR_SET_PRUEBAS);

                    Console.WriteLine("ZipKey: {0}", uploadResponse.ZipKey);

                    if (uploadResponse.ErrorMessageList != null)
                    {
                        foreach (var itemList in uploadResponse.ErrorMessageList)
                        {
                            Console.WriteLine("[{0}] {1}", itemList.DocumentKey, itemList.ProcessedMessage);
                        }
                    }


                    Console.WriteLine("Consultando validaciones...");

                    dianResponse = clienteDian.ConsultarDocumentos(uploadResponse.ZipKey)[0];

                    string registro = string.Format("\r\nNOTA_DEBITO\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", debitNote.IssueDate.Value,
                        debitNote.ID.Value, debitNote.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }

                else if (AMBIENTE_PRUEBAS)
                {
                    dianResponse = clienteDian.EnviarDocumento(signedBytes);

                    string registro = string.Format("\r\nNOTA_DEBITO\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", debitNote.IssueDate.Value,
                        debitNote.ID.Value, debitNote.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }
                else
                {
                    dianResponse = clienteDian.EnviarDocumento(signedBytes);

                    string registro = string.Format("\r\nNOTA_DEBITO\t\t{0}\t\t{1}\t\t{2}\t\t{3}: {4}", debitNote.IssueDate.Value,
                        debitNote.ID.Value, debitNote.UUID.Value, dianResponse.StatusCode, dianResponse.StatusMessage);
                    File.AppendAllText(ARCHIVO_HISTORIAL, registro + "\r\n");

                    Console.WriteLine("{0} - {1} - {2}", dianResponse.StatusCode, dianResponse.StatusMessage, dianResponse.StatusDescription);
                    if (dianResponse.StatusCode != RespuestaDian.PROCESADO_CORRECTAMENTE)
                    {
                        if (dianResponse.ErrorMessage != null)
                        {
                            File.AppendAllLines(ARCHIVO_HISTORIAL, dianResponse.ErrorMessage);
                            Console.WriteLine(string.Join("\r\n", dianResponse.ErrorMessage));
                        }
                    }

                    var appResponse = (dianResponse.XmlBase64Bytes == null) ? "null" : Encoding.UTF8.GetString(dianResponse.XmlBase64Bytes);

                    File.WriteAllText("docs/" + numero + "-appresponse.xml", appResponse);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return;
            }



            Console.WriteLine("Creando pdf...");

            try
            {
                var notaDebitoPdf = new NotaDebitoPdf(debitNote)
                {
                    RutaLogo = "logo.png",
                    TextoEncabezado = "Persona Jurídica \r\nRégimen Ordinario",
                    TextoQR = debitNote.UUID.Value,
                    TextoConstancia = DocumentoPdf.TEXTO_CONSTANCIA_NOTA_DEBITO_DEFAULT,
                    TextoResolucion = DocumentoPdf.CrearTextoResolucion(RANGO_NUMERO_RESOLUCION,
                        RANGO_FECHA_RESOLUCION, RANGO_PREFIJO, RANGO_DESDE, RANGO_HASTA, RANGO_VIGENCIA_HASTA)
                };

                var bytesPdf = notaDebitoPdf.Generar();

                File.WriteAllBytes("docs/" + numero + ".pdf", bytesPdf);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static void TestAcuseRecibo(InvoiceType invoice)
        {
//            if (invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.Value != EMISOR_NIT)
//            {
//                Console.WriteLine();
//                Console.WriteLine("Solo se pueden generar eventos para documentos recibidos.");
//                return;
//            }

            Console.WriteLine();
            Console.WriteLine("Creando evento 'acuse de recibo'...");

            var fecha = DateTimeHelper.GetColombianDate();

            var extensionDian = new ExtensionDian
            {
                PaisOrigen = Pais.COLOMBIA,
                SoftwareProveedorNit = EMISOR_NIT,
                SoftwareIdentificador = SOFTWARE_IDENTIFICADOR,
                SoftwarePin = SOFTWARE_PIN
            };

            var referencia = new ReferenciaDocumento
            {
                Numero = invoice.ID.Value,
                AlgoritmoCufe = new AlgoritmoSeguridadUUID { Codigo = invoice.UUID.schemeName },
                Cufe = invoice.UUID.Value,
                TipoDocumento = new TipoDocumento { Codigo = invoice.InvoiceTypeCode.Value },
                Fecha = invoice.IssueDate.Value
            };

            var emisor = new ParteEvento
            {
                TipoIdentificacion = new TipoIdentificacion
                {
                    Codigo = invoice.AccountingSupplierParty.Party.PartyTaxScheme[0].CompanyID.schemeName
                },
                Identificacion = invoice.AccountingSupplierParty.Party.PartyTaxScheme[0].CompanyID.Value,
                Nombre = invoice.AccountingSupplierParty.Party.PartyName[0].Name.Value
            };

            var adquiriente = new ParteEvento
            {
                TipoIdentificacion = new TipoIdentificacion
                {
                    Codigo = invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.schemeName
                },
                Identificacion = invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.Value,
                Nombre = invoice.AccountingCustomerParty[0].Party.PartyName[0].Name.Value
            };

            var generador = new GeneradorEvento(AMBIENTE_DESTINO, referencia)
                .ConFecha(fecha)
                .ConExtensionDian(extensionDian)
                .ConRegistrante(adquiriente)
                .ConReceptor(emisor)
                .ParaAcuseRecibo()
                .AsignarCUDE(SOFTWARE_PIN);

            var applicationResponse = generador.Obtener();

            var xmlApplicationResponse = applicationResponse.SerializeXmlDocument();

            var stringApplicationResponse = xmlApplicationResponse.ToXmlString();

            

            Console.WriteLine("Firmando...");

            var firma = new FirmaElectronica
            {
                RolFirmante = RolFirmante.EMISOR,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            fecha = DateTimeHelper.GetColombianDate();

            var signedApplicationResponse = firma.FirmarEvento(stringApplicationResponse, fecha);

            stringApplicationResponse = Encoding.UTF8.GetString(signedApplicationResponse);
            


            Console.WriteLine("Emitiendo...");

            var clienteDian = new ClienteServicioDian
            {
                Ambiente = AMBIENTE_SERVICIO,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            EventResponse[] response;
            try
            {
                response = clienteDian.EnviarEvento(signedApplicationResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return;
            }

            foreach (var item in response)
                Console.WriteLine("{0} - {1}", item.Code, item.Message);
        }

        static void TestRechazoDocumento(InvoiceType invoice)
        {
            if (invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.Value != EMISOR_NIT)
            {
                Console.WriteLine();
                Console.WriteLine("Solo se pueden generar eventos para documentos recibidos.");
                return;
            }

            Console.WriteLine();
            Console.WriteLine("Creando evento 'rechazo de documento'...");

            var fecha = DateTimeHelper.GetColombianDate();

            var extensionDian = new ExtensionDian
            {
                PaisOrigen = Pais.COLOMBIA,
                SoftwareProveedorNit = EMISOR_NIT,
                SoftwareIdentificador = SOFTWARE_IDENTIFICADOR,
                SoftwarePin = SOFTWARE_PIN
            };

            var referencia = new ReferenciaDocumento
            {
                Numero = invoice.ID.Value,
                AlgoritmoCufe = new AlgoritmoSeguridadUUID { Codigo = invoice.UUID.schemeName },
                Cufe = invoice.UUID.Value,
                TipoDocumento = new TipoDocumento { Codigo = invoice.InvoiceTypeCode.Value },
                Fecha = invoice.IssueDate.Value
            };

            var emisor = new ParteEvento
            {
                TipoIdentificacion = new TipoIdentificacion
                {
                    Codigo = invoice.AccountingSupplierParty.Party.PartyTaxScheme[0].CompanyID.schemeName
                },
                Identificacion = invoice.AccountingSupplierParty.Party.PartyTaxScheme[0].CompanyID.Value,
                Nombre = invoice.AccountingSupplierParty.Party.PartyName[0].Name.Value
            };

            var adquiriente = new ParteEvento
            {
                TipoIdentificacion = new TipoIdentificacion
                {
                    Codigo = invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.schemeName
                },
                Identificacion = invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.Value,
                Nombre = invoice.AccountingCustomerParty[0].Party.PartyName[0].Name.Value
            };

            var generador = new GeneradorEvento(AMBIENTE_DESTINO, referencia)
                .ConFecha(fecha)
                .ConExtensionDian(extensionDian)
                .ConRegistrante(adquiriente)
                .ConReceptor(emisor)
                .ParaRechazoDocumento()
                .AsignarCUDE(SOFTWARE_PIN);

            var applicationResponse = generador.Obtener();

            var xmlApplicationResponse = applicationResponse.SerializeXmlDocument();

            var stringApplicationResponse = xmlApplicationResponse.ToXmlString();



            Console.WriteLine("Firmando...");

            var firma = new FirmaElectronica
            {
                RolFirmante = RolFirmante.EMISOR,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            fecha = DateTimeHelper.GetColombianDate();

            var signedApplicationResponse = firma.FirmarEvento(stringApplicationResponse, fecha);

            stringApplicationResponse = Encoding.UTF8.GetString(signedApplicationResponse);



            Console.WriteLine("Emitiendo...");

            var clienteDian = new ClienteServicioDian
            {
                Ambiente = AMBIENTE_SERVICIO,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            EventResponse[] response;
            try
            {
                response = clienteDian.EnviarEvento(signedApplicationResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return;
            }

            foreach (var item in response)
                Console.WriteLine("{0} - {1}", item.Code, item.Message);
        }

        static void TestRecepcionBienes(InvoiceType invoice)
        {
            if (invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.Value != EMISOR_NIT)
            {
                Console.WriteLine();
                Console.WriteLine("Solo se pueden generar eventos para documentos recibidos.");
                return;
            }

            Console.WriteLine();
            Console.WriteLine("Creando evento 'recepción de bienes'...");

            var fecha = DateTimeHelper.GetColombianDate();

            var extensionDian = new ExtensionDian
            {
                PaisOrigen = Pais.COLOMBIA,
                SoftwareProveedorNit = EMISOR_NIT,
                SoftwareIdentificador = SOFTWARE_IDENTIFICADOR,
                SoftwarePin = SOFTWARE_PIN
            };

            var referencia = new ReferenciaDocumento
            {
                Numero = invoice.ID.Value,
                AlgoritmoCufe = new AlgoritmoSeguridadUUID { Codigo = invoice.UUID.schemeName },
                Cufe = invoice.UUID.Value,
                TipoDocumento = new TipoDocumento { Codigo = invoice.InvoiceTypeCode.Value },
                Fecha = invoice.IssueDate.Value
            };

            var emisor = new ParteEvento
            {
                TipoIdentificacion = new TipoIdentificacion
                {
                    Codigo = invoice.AccountingSupplierParty.Party.PartyTaxScheme[0].CompanyID.schemeName
                },
                Identificacion = invoice.AccountingSupplierParty.Party.PartyTaxScheme[0].CompanyID.Value,
                Nombre = invoice.AccountingSupplierParty.Party.PartyName[0].Name.Value
            };

            var adquiriente = new ParteEvento
            {
                TipoIdentificacion = new TipoIdentificacion
                {
                    Codigo = invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.schemeName
                },
                Identificacion = invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.Value,
                Nombre = invoice.AccountingCustomerParty[0].Party.PartyName[0].Name.Value
            };

            var generador = new GeneradorEvento(AMBIENTE_DESTINO, referencia)
                .ConFecha(fecha)
                .ConExtensionDian(extensionDian)
                .ConRegistrante(adquiriente)
                .ConReceptor(emisor)
                .ParaRecepcionBienes()
                .AsignarCUDE(SOFTWARE_PIN);

            var applicationResponse = generador.Obtener();

            var xmlApplicationResponse = applicationResponse.SerializeXmlDocument();

            var stringApplicationResponse = xmlApplicationResponse.ToXmlString();



            Console.WriteLine("Firmando...");

            var firma = new FirmaElectronica
            {
                RolFirmante = RolFirmante.EMISOR,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            fecha = DateTimeHelper.GetColombianDate();

            var signedApplicationResponse = firma.FirmarEvento(stringApplicationResponse, fecha);

            stringApplicationResponse = Encoding.UTF8.GetString(signedApplicationResponse);



            Console.WriteLine("Emitiendo...");

            var clienteDian = new ClienteServicioDian
            {
                Ambiente = AMBIENTE_SERVICIO,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            EventResponse[] response;
            try
            {
                response = clienteDian.EnviarEvento(signedApplicationResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return;
            }

            foreach (var item in response)
                Console.WriteLine("{0} - {1}", item.Code, item.Message);
        }

        static void TestAceptacionDocumento(InvoiceType invoice)
        {
            if (invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.Value != EMISOR_NIT)
            {
                Console.WriteLine();
                Console.WriteLine("Solo se pueden generar eventos para documentos recibidos.");
                return;
            }

            Console.WriteLine();
            Console.WriteLine("Creando evento 'aceptación de documento'...");

            var fecha = DateTimeHelper.GetColombianDate();

            var extensionDian = new ExtensionDian
            {
                PaisOrigen = Pais.COLOMBIA,
                SoftwareProveedorNit = EMISOR_NIT,
                SoftwareIdentificador = SOFTWARE_IDENTIFICADOR,
                SoftwarePin = SOFTWARE_PIN
            };

            var referencia = new ReferenciaDocumento
            {
                Numero = invoice.ID.Value,
                AlgoritmoCufe = new AlgoritmoSeguridadUUID { Codigo = invoice.UUID.schemeName },
                Cufe = invoice.UUID.Value,
                TipoDocumento = new TipoDocumento { Codigo = invoice.InvoiceTypeCode.Value },
                Fecha = invoice.IssueDate.Value
            };

            var emisor = new ParteEvento
            {
                TipoIdentificacion = new TipoIdentificacion
                {
                    Codigo = invoice.AccountingSupplierParty.Party.PartyTaxScheme[0].CompanyID.schemeName
                },
                Identificacion = invoice.AccountingSupplierParty.Party.PartyTaxScheme[0].CompanyID.Value,
                Nombre = invoice.AccountingSupplierParty.Party.PartyName[0].Name.Value
            };

            var adquiriente = new ParteEvento
            {
                TipoIdentificacion = new TipoIdentificacion
                {
                    Codigo = invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.schemeName
                },
                Identificacion = invoice.AccountingCustomerParty[0].Party.PartyTaxScheme[0].CompanyID.Value,
                Nombre = invoice.AccountingCustomerParty[0].Party.PartyName[0].Name.Value
            };

            var generador = new GeneradorEvento(AMBIENTE_DESTINO, referencia)
                .ConFecha(fecha)
                .ConExtensionDian(extensionDian)
                .ConRegistrante(adquiriente)
                .ConReceptor(emisor)
                .ParaAceptacionDocumento()
                .AsignarCUDE(SOFTWARE_PIN);

            var applicationResponse = generador.Obtener();

            var xmlApplicationResponse = applicationResponse.SerializeXmlDocument();

            var stringApplicationResponse = xmlApplicationResponse.ToXmlString();



            Console.WriteLine("Firmando...");

            var firma = new FirmaElectronica
            {
                RolFirmante = RolFirmante.EMISOR,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            fecha = DateTimeHelper.GetColombianDate();

            var signedApplicationResponse = firma.FirmarEvento(stringApplicationResponse, fecha);

            stringApplicationResponse = Encoding.UTF8.GetString(signedApplicationResponse);



            Console.WriteLine("Emitiendo...");

            var clienteDian = new ClienteServicioDian
            {
                Ambiente = AMBIENTE_SERVICIO,
                RutaCertificado = RUTA_CERTIFICADO,
                ClaveCertificado = CLAVE_CERTIFICADO
            };

            EventResponse[] response;
            try
            {
                response = clienteDian.EnviarEvento(signedApplicationResponse);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);

                return;
            }

            foreach (var item in response)
                Console.WriteLine("{0} - {1}", item.Code, item.Message);
        }
    }
}