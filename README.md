# eFacturacionColombia_V2 [![version](https://img.shields.io/badge/version-2.3-blue.svg)](#) [![build](https://img.shields.io/badge/build-passing-brightgreen.svg)](#)

Repositorio para actualizaciones de librería eFacturacionColombia_V2 (.NET). No compartir contenido.

------
** Para instalar

- Clone el proyecto
- Abra la solucion con Visual Studio
- Limpie la solucion
- Compile la solucion

** Si aparece un error de ZipArchive o de System.io.compression

- Abra el archivo C:\eFacturacionColombia_V2_facturalatam\eFacturacionColombia_V2.Util\eFacturacionColombia_V2.Util.csproj
- Modifique las lineas que se indican para que queden de la siguiente manera:

		<Reference Include="System.IO.Compression, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089, processorArchitecture=MSIL" />
		<Reference Include="System.IO.Compression.FileSystem, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089, processorArchitecture=MSIL" />

- Vuelva a abrir la solucion
- Compile de nuevo, ya no aparecera el error.		
